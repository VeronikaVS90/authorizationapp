from django.urls import path, include

from rest_framework import routers

from . import views

app_name = "user_app"

# routers
router = routers.DefaultRouter(trailing_slash=False)

# registration
router.register("", views.CustomUserViewSet, basename="user")

urlpatterns = [
    path('', include(router.urls)),
]
